/* eslint-disable no-undef */
'use strict';
var page = require('webpage').create();
var pageurl='http://www.lyceecolbert-tg.org/menu-vivre/restauration-scolaire.html';

page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onResourceRequested = function(requestData, networkRequest) {
    if (requestData.url !== pageurl) networkRequest.abort() && console.log('aborted :' + requestData.url);
};

page.onError = function() {};

page.open(pageurl, function(status) {
    if (status === 'success') {
        page.evaluate(function() {
            var property = ['day', 'opening', 'main', 'desert'];
            var tableBody = document.querySelector('.contenttable>tbody');
            var trs = Array.prototype.slice.call(tableBody.getElementsByTagName('tr')).filter(function(elem) {
                return elem.childNodes.length === 5;
            });
            var result = [{}, {}, {}, {}, {}];
            for (var i = 0, len = trs.length; i < len; i++) {
                var tr = trs[i];
                var tds = Array.prototype.slice.call(tr.childNodes);
                for (var j = 0, len2 = tds.length; j < len2; j++) {
                    var text = '';
                    var ps = Array.prototype.slice.call(tds[j].getElementsByTagName('p'));
                    for (var k = 0, len3 = ps.length; k < len3; k++) {
                        var p = ps[k];
                        text += p.textContent.replace(/\n/g, '') + '\n';
                    }
                    result[j][property[i]] = text.replace(/(\s)+/g, '$1').replace(/\n$|^\n/g, '').toLowerCase().replace(/\n./g, function(match) {
                        return match.toUpperCase();
                    }).replace(/./, function(match) {
                        return match.toUpperCase();
                    });
                }
            }
            console.log(JSON.stringify(result));
        });
        phantom.exit(0);
    } else {
        phantom.exit(1);
    }
});
